# it.opbyte.superdownload
两个Activity:
- MainActivity.java(关键)
- Preferences.java

一个Service:
- MainActivity$GetService

关键在于
`MainActivity`以及
`MainActivity$GetService`

代码分析见[MainActivity.java](apk/src/MainActivity.java)

## 流程
启动之后，进入MainActivity，右上角有三个菜单，如图：

<img src="screenshot/MainActivity.png" width=20% height=20%>
</br>
点击加号，进入"Enter URL to download"对话框，
</br>
<img src="screenshot/Enter_URL_To_Download.png" width=20% height=20%>
</br>
输入URL之后，
</br>
<img src="screenshot/to_be_download.png" width=20% height=20%>
</br>
点击确定，开始下载；
</br>
<img src="screenshot/downloading.png" width=20% height=20%>
</br>
点击取消，什么都不做。</br>


