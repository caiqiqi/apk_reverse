package it.opbyte.superdownload;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class Preferences extends PreferenceActivity {
    public Preferences() {
        super();
    }

    @Override public void onCreate(Bundle arg2) {
        super.onCreate(arg2);
        this.addPreferencesFromResource(2130968576);
    }
}


