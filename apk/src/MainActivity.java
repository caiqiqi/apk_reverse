package it.opbyte.superdownload;

import android.app.Activity;
import android.app.AlertDialog$Builder;
import android.app.DownloadManager$Request;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface$OnClickListener;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences$Editor;
import android.content.SharedPreferences$OnSharedPreferenceChangeListener;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo$State;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build$VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.StatFs;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ProgressBar;
import android.widget.RemoteViews;
import android.widget.TextView;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

public class MainActivity extends Activity {

	/*唯一的广播接收者*/
    class MyBroadcastReceiver extends BroadcastReceiver {
        private final MainActivity this$0;

        MyBroadcastReceiver(MainActivity arg1) {
            super();
            MainActivity.this = arg1;
        }
        // 带$符合的说明都是内部类产生的类
        // access$0 为由外部调用的合成方法
        static MainActivity access$0(MyBroadcastReceiver arg1) {
            return arg1.this$0;
        }

        @Override
        // BroadcastReceiver类接收到广播之后的回调函数
        public void onReceive(Context context, Intent intent) {
            if(DownloadManager.ACTION_NOTIFICATION_CLICKED.equals(intent.getAction())) {
                MainActivity.this.showHistory(context);
            }
        }
    }

/*监听Preference的改变，并做出响应*/
    class MyOnSharedPreferenceChangeListener implements SharedPreferences$OnSharedPreferenceChangeListener {
        private final MainActivity this$0;

        MyOnSharedPreferenceChangeListener(MainActivity arg1) {
            super();
            MainActivity.this = arg1;
        }
        /*为了MainActivity能够访问，不用管*/
        static MainActivity access$0(MyOnSharedPreferenceChangeListener arg1) {
            return arg1.this$0;
        }

        public void onSharedPreferenceChanged(SharedPreferences arg2, String arg3) {
            MainActivity.this.ReadPreferences();
        }
    }

/*第一个 DialogInterface$OnClickListener
 *点击按钮之后结束对话框
 */
    class zDialogInterface_OnClickListener implements DialogInterface$OnClickListener {
        private final MainActivity this$0;

        zDialogInterface_OnClickListener(MainActivity arg1) {
            super();
            MainActivity.this = arg1;
        }
        /*为了MainActivity能够访问，不用管*/
        static MainActivity access$0(zDialogInterface_OnClickListener arg1) {
            return arg1.this$0;
        }
        /*点击按钮之后结束对话框*/
        public void onClick(DialogInterface arg1, int arg2) {
            arg1.cancel();
        }
    }

/*第二个 DialogInterface$OnClickListener
 * 点击确定按钮之后，调用Restart方法，将url传给GetService，准备下载
 */
    class yDialogInterface_OnClickListener implements DialogInterface$OnClickListener {
        private final MainActivity this$0;
        private final Context val$context;
        private final View val$layout;

        yDialogInterface_OnClickListener(MainActivity arg1, View arg2, Context arg3) {
            super();
            MainActivity.this = arg1;
            this.val$layout = arg2;
            this.val$context = arg3;
        }
        /*为了MainActivity能够访问，不用管*/
        static MainActivity access$0(yDialogInterface_OnClickListener arg1) {
            return arg1.this$0;
        }

        public void onClick(DialogInterface arg4, int arg5) {
            MainActivity.this.Restart(this.val$context, this.val$layout.findViewById(2131296257).getText().toString().trim());
            MainActivity.this.setResult(-1);
        }
    }

/*第三个 DialogInterface$OnClickListener
 * 直接结束该对话框
 */
    class xDialogInterface_OnClickListener implements DialogInterface$OnClickListener {
        private final MainActivity this$0;

        xDialogInterface_OnClickListener(MainActivity arg1) {
            super();
            MainActivity.this = arg1;
        }
        /*为了MainActivity能够访问，不用管*/
        static MainActivity access$0(xDialogInterface_OnClickListener arg1) {
            return arg1.this$0;
        }

        public void onClick(DialogInterface arg1, int arg2) {
            arg1.cancel();
        }
    }

/*第四个 DialogInterface$OnClickListener
 * 直接结束该对话框
 */
    class wDialogInterface_OnClickListener implements DialogInterface$OnClickListener {
        private final MainActivity this$0;

        wDialogInterface_OnClickListener(MainActivity arg1) {
            super();
            MainActivity.this = arg1;
        }
        /*为了MainActivity能够访问，不用管*/
        static MainActivity access$0(wDialogInterface_OnClickListener arg1) {
            return arg1.this$0;
        }

        public void onClick(DialogInterface arg1, int arg2) {
            arg1.cancel();
        }
    }

/*第五个 DialogInterface$OnClickListener
 * 直接结束该对话框
 */
    class vDialogInterface_OnClickListener implements DialogInterface$OnClickListener {
        private final MainActivity this$0;

        vDialogInterface_OnClickListener(MainActivity arg1) {
            super();
            MainActivity.this = arg1;
        }
        /*为了MainActivity能够访问，不用管*/
        static MainActivity access$0(vDialogInterface_OnClickListener arg1) {
            return arg1.this$0;
        }

        public void onClick(DialogInterface arg1, int arg2) {
            arg1.cancel();
        }
    }

/*第六个 DialogInterface$OnClickListener*/
    class uDialogInterface_OnClickListener implements DialogInterface$OnClickListener {
        private final MainActivity this$0;
        private final Context val$context;

        uDialogInterface_OnClickListener(MainActivity arg1, Context arg2) {
            super();
            MainActivity.this = arg1;
            this.val$context = arg2;
        }
        /*为了MainActivity能够访问，不用管*/
        static MainActivity access$0(uDialogInterface_OnClickListener arg1) {
            return arg1.this$0;
        }

        public void onClick(DialogInterface arg7, int arg8) {
            Intent v0;
            Class v2;
            int v5 = 8;
            int v4 = 2;
            MainActivity.this.setResult(-1);
            if(MainActivity.status != v4) {
                if(MainActivity.status != 1 && MainActivity.status != 3 && MainActivity.status != 4) {
                    if(MainActivity.status == v5) {
                        MainActivity.status = arg8 == 0 ? 0 : 1;
                        Context context = this.val$context;
                        try {
                            v2 = Class.forName("it.opbyte.superdownload.MainActivity$GetService");
                        }
                        catch(ClassNotFoundException v0_1) {
                            throw new NoClassDefFoundError(((Throwable)v0_1).getMessage());
                        }

                        v0 = new Intent(context, v2);
                        v0.setAction(".ACTION_RESUME");
                        v0.putExtra("url", MainActivity.url_orig);
                        MainActivity.this.startService(v0);
                    }
                    else {
                        if(arg8 == 0) {
                            MainActivity.this.Restart(this.val$context, MainActivity.url_orig);
                            return;
                        }

                        if(arg8 == 1) {
                            MainActivity.this.SendToAndroidDownloader(this.val$context, MainActivity.url_orig);
                            return;
                        }

                        MainActivity.this.clear();
                    }

                    return;
                }

                if(arg8 == 0) {
                    MainActivity.status = 0;
                    return;
                }

                MainActivity.status = v5;
            }
            else if(arg8 == 0) {
                v0 = new Intent("android.intent.action.VIEW");
                Uri v1 = Uri.fromFile(new File(MainActivity.file_down));
                if(MainActivity.mime_type.length() == 0) {
                    MainActivity.mime_type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(v1.toString()));
                }

                v0.setDataAndType(v1, MainActivity.mime_type);
                this.val$context.startActivity(Intent.createChooser(v0, ""));
            }
            else {
                if(arg8 == 1) {
                    MainActivity.this.Restart(this.val$context, MainActivity.url_orig);
                    return;
                }

                if(arg8 == v4) {
                    MainActivity.this.SendToAndroidDownloader(this.val$context, MainActivity.url_orig);
                    return;
                }

                MainActivity.this.clear();
            }
        }
    }

/*第七个 DialogInterface$OnClickListener*/
    class tDialogInterface_OnClickListener implements DialogInterface$OnClickListener {
        private final MainActivity this$0;
        private final Context val$context;
        private final String val$fname;

        tDialogInterface_OnClickListener(MainActivity arg1, Context arg2, String arg3) {
            super();
            MainActivity.this = arg1;
            this.val$context = arg2;
            this.val$fname = arg3;
        }
        /*为了MainActivity能够访问，不用管*/
        static MainActivity access$0(tDialogInterface_OnClickListener arg1) {
            return arg1.this$0;
        }

        public void onClick(DialogInterface arg4, int arg5) {
            MainActivity.this.Restart(this.val$context, this.val$fname);
            arg4.cancel();
        }
    }

/*第八个 DialogInterface$OnClickListener*/
    class sDialogInterface_OnClickListener implements DialogInterface$OnClickListener {
        private final MainActivity this$0;
        private final String val$fpath;

        sDialogInterface_OnClickListener(MainActivity arg1, String arg2) {
            super();
            MainActivity.this = arg1;
            this.val$fpath = arg2;
        }
        /*为了MainActivity能够访问，不用管*/
        static MainActivity access$0(sDialogInterface_OnClickListener arg1) {
            return arg1.this$0;
        }

        public void onClick(DialogInterface arg2, int arg3) {
            MainActivity.DeleteTemp(this.val$fpath);
            arg2.cancel();
        }
    }


/*唯一的Service，重点！*/
    public class GetService extends Service {
        class Runnable1 implements Runnable {
            private final Context val$context;
            private final String val$url;

            Runnable1(Context arg1, String arg2) {
                super();
                this.val$context = arg1;
                this.val$url = arg2;
            }

            public void run() {
                Class v2_3;
                int v1_2;
                BufferedInputStream v0_2;
                BufferedOutputStream v5;
                BufferedOutputStream v1 = null;
                Collections.sort(MainActivity.segments, new segmentComparator());
                try {
                    MainActivity.file_down = MainActivity.getFileName(MainActivity.file_name);
                    v5 = new BufferedOutputStream(new FileOutputStream(MainActivity.file_down));
                }
                catch(FileNotFoundException v2) {
                    v2.printStackTrace();
                    v5 = v1;
                }

                if(v5 != null) {
                    byte[] v6 = new byte[MainActivity.buffer_size];
                    int v3 = 0;
                    int v2_1 = 0;
                    while(v3 < MainActivity.segments.size()) {
                        Object v0 = MainActivity.segments.get(v3);
                        if(!((Segment)v0).failed) {
                            try {
                                v0_2 = new BufferedInputStream(new FileInputStream(new StringBuffer().append(new StringBuffer().append(MainActivity.temp_file_dir).append("/").toString()).append(((Segment)v0).start).toString()));
                            }
                            catch(FileNotFoundException v0_1) {
                                v0_1.printStackTrace();
                                v0_2 = ((BufferedInputStream)v1);
                            }

                            try {
                                v2_1 = v0_2.read(v6, 0, MainActivity.buffer_size);
                                v0_2.close();
                                v1_2 = v2_1;
                            }
                            catch(IOException v1_1) {
                                v1_1.printStackTrace();
                                v1_2 = v2_1;
                            }

                            try {
                                v5.write(v6, 0, v1_2);
                            }
                            catch(IOException v2_2) {
                                v2_2.printStackTrace();
                            }
                        }
                        else {
                            v0_2 = ((BufferedInputStream)v1);
                            v1_2 = v2_1;
                        }

                        ++v3;
                        v2_1 = v1_2;
                        BufferedInputStream v1_3 = v0_2;
                    }

                    try {
                        v5.close();
                    }
                    catch(IOException v0_3) {
                        v0_3.printStackTrace();
                    }

                    MainActivity.DeleteTemp(MainActivity.temp_file_dir);
                }

                Context v1_4 = this.val$context;
                try {
                    v2_3 = Class.forName("it.opbyte.superdownload.MainActivity$GetService");
                }
                catch(ClassNotFoundException v0_4) {
                    throw new NoClassDefFoundError(((Throwable)v0_4).getMessage());
                }

                Intent v0_5 = new Intent(v1_4, v2_3);
                v0_5.setAction(".ACTION_FINISHED");
                v0_5.putExtra("url", this.val$url);
                this.val$context.startService(v0_5);
            }
        }

        class Runnable2 implements Runnable {
            private final Context val$context;
            private final long val$end;
            private final boolean val$mobile;
            private final boolean val$mobile_true;
            private final int val$segment;
            private final long val$start;
            private final String val$url;

            Runnable2(boolean arg1, Context arg2, String arg3, long arg4, long arg6, int arg8, boolean arg9) {
                super();
                this.val$mobile_true = arg1;
                this.val$context = arg2;
                this.val$url = arg3;
                this.val$start = arg4;
                this.val$end = arg6;
                this.val$segment = arg8;
                this.val$mobile = arg9;
            }

            public void run() {
                Class v3;
                BufferedOutputStream v1_2;
                Exception v1;
                byte[] v0_1;
                int v10 = 3;
                byte[] v8 = new byte[0];
                if(this.val$mobile_true) {
                    if(MainActivity.status != 1 && MainActivity.status != v10) {
                        goto label_13;
                    }

                    MainActivity.forceMobileConnectionForAddress(this.val$context, this.val$url);
                }

                try {
                label_13:
                    v0_1 = GetService.getStringFromUrl(this.val$context, this.val$url, this.val$start, this.val$end, this.val$segment, this.val$mobile_true);
                }
                catch(Exception v0) {
                    v1 = v0;
                    v0_1 = v8;
                    goto label_91;
                }

                try {
                    if(v0_1.length <= 0) {
                        goto label_53;
                    }

                    if(MainActivity.status != 1 && MainActivity.status != v10 && MainActivity.status != 4) {
                        if(MainActivity.status == 8) {
                            goto label_32;
                        }

                        goto label_53;
                    }
                }
                catch(Exception v1) {
                    goto label_91;
                }

            label_32:
                BufferedOutputStream v2 = null;
                try {
                    v1_2 = new BufferedOutputStream(new FileOutputStream(new StringBuffer().append(new StringBuffer().append(MainActivity.temp_file_dir).append("/").toString()).append(this.val$start).toString()));
                    goto label_50;
                }
                catch(Exception v1) {
                }
                catch(FileNotFoundException v1_1) {
                    try {
                        v1_1.printStackTrace();
                        v1_2 = v2;
                    }
                    catch(Exception v1) {
                        goto label_91;
                    }

                label_50:
                    if(v1_2 == null) {
                        goto label_53;
                    }

                    try {
                        v1_2.write(v0_1);
                        v1_2.close();
                        goto label_53;
                    }
                    catch(Exception v1) {
                    }
                    catch(IOException v1_3) {
                        try {
                            v1_3.printStackTrace();
                            goto label_53;
                        }
                        catch(Exception v1) {
                        }
                    }
                }

            label_91:
                v1.printStackTrace();
            label_53:
                Context v2_1 = this.val$context;
                try {
                    v3 = Class.forName("it.opbyte.superdownload.MainActivity$GetService");
                }
                catch(ClassNotFoundException v0_2) {
                    throw new NoClassDefFoundError(((Throwable)v0_2).getMessage());
                }

                Intent v1_4 = new Intent(v2_1, v3);
                v1_4.setAction(".ACTION_GETURL_DONE");
                v1_4.putExtra("segment", this.val$segment);
                v1_4.putExtra("url", this.val$url);
                v1_4.putExtra("start", this.val$start);
                v1_4.putExtra("end", this.val$start + (((long)v0_1.length)) - (((long)1)));
                v1_4.putExtra("mobile", this.val$mobile);
                this.val$context.startService(v1_4);
            }
        }

        public GetService() {
            super();
        }

        static byte[] access$1000020(Context arg1, String arg2, long arg3, long arg5, int arg7, boolean arg8) {
            return GetService.getStringFromUrl(arg1, arg2, arg3, arg5, arg7, arg8);
        }

        private static void compactSegments(Context arg2, String arg3) {
            Thread v0 = new Thread(new Runnable1(arg2, arg3));
            v0.setPriority(3);
            v0.start();
        }

        private static InputStream getInputStreamFromUrl(String arg10, long arg11, long arg13, int arg15, boolean arg16) {
            InputStream v0_6;
            InputStream v1 = null;
            try {
                BasicHttpParams v2 = new BasicHttpParams();
                HttpProtocolParams.setVersion(((HttpParams)v2), HttpVersion.HTTP_1_1);
                int v0_1 = arg16 ? 3000 : 5000;
                HttpConnectionParams.setConnectionTimeout(((HttpParams)v2), v0_1);
                v0_1 = arg16 ? 3500 : 6000;
                HttpConnectionParams.setSoTimeout(((HttpParams)v2), v0_1);
                HttpConnectionParams.setTcpNoDelay(((HttpParams)v2), false);
                HttpConnectionParams.setStaleCheckingEnabled(((HttpParams)v2), false);
                InetAddress v0_2 = MainActivity.getAddressByType(arg16);
                if(v0_2 != null) {
                    ConnRouteParams.setLocalAddress(((HttpParams)v2), v0_2);
                }

                DefaultHttpClient v0_3 = new DefaultHttpClient(((HttpParams)v2));
                HttpGet v2_1 = new HttpGet(arg10);
                if(arg11 > (((long)0))) {
                    v2_1.addHeader("Range", new StringBuffer().append(new StringBuffer().append(new StringBuffer().append("bytes=").append(arg11).toString()).append("-").toString()).append(arg13).toString());
                }

                HttpResponse v2_2 = v0_3.execute(((HttpUriRequest)v2_1));
                if(arg11 == (((long)0))) {
                    Header[] v3 = v2_2.getAllHeaders();
                    for(v0_1 = 0; v0_1 < v3.length; ++v0_1) {
                        Log.d("SuperDownload", new StringBuffer().append(new StringBuffer().append(new StringBuffer().append("Header found: ").append(v3[v0_1].getName()).toString()).append(" value: ").toString()).append(v3[v0_1].getValue()).toString());
                    }

                    Header v0_4 = v2_2.getFirstHeader("Content-Length");
                    if(v0_4 != null) {
                        MainActivity.size_total = Long.valueOf(v0_4.getValue()).longValue();
                        Log.d("SuperDownload", new StringBuffer().append("Got a file size from header: ").append(MainActivity.size_total).toString());
                    }
                    else {
                        Log.e("SuperDownload", "No content length header");
                    }

                    v0_4 = v2_2.getFirstHeader("Content-Disposition");
                    if(v0_4 != null) {
                        String v0_5 = v0_4.getValue();
                        int v3_1 = v0_5.lastIndexOf(61);
                        if(v3_1 != -1) {
                            MainActivity.file_name = v0_5.substring(v3_1 + 1);
                            Log.d("SuperDownload", new StringBuffer().append("Got file name from header: ").append(MainActivity.file_name).toString());
                        }
                    }

                    v0_4 = v2_2.getFirstHeader("Content-Type");
                    if(v0_4 == null) {
                        goto label_102;
                    }

                    MainActivity.mime_type = v0_4.getValue();
                    Log.d("SuperDownload", new StringBuffer().append("Got mime type from header: ").append(MainActivity.mime_type).toString());
                }

            label_102:
                if(MainActivity.size_total >= MainActivity.max_size) {
                    Log.e("SuperDownload", new StringBuffer().append("Size is bigger than maximum of ").append(MainActivity.max_size).toString());
                    v0_6 = v1;
                }
                else {
                    v0_6 = v2_2.getEntity().getContent();
                }

                return v0_6;
            }
            catch(Exception v0) {
                v0.printStackTrace();
                return v1;
            }
        }

        private static int getSegment() {
            int v0_1;
            Object v0;
            int v3;
            long v1;
            int v4 = 0;
            long v5 = ((long)0);
            if(MainActivity.size_total == (((long)0))) {
                v1 = ((long)0);
                v5 = ((long)(MainActivity.buffer_size - 1));
                goto label_10;
            }
            else {
                v1 = ((long)0);
                while(true) {
                label_20:
                    if(v1 < MainActivity.size_total) {
                        v3 = 0;
                        while(true) {
                        label_45:
                            if(v3 < MainActivity.segments.size()) {
                                v0 = MainActivity.segments.get(v3);
                                if(!((Segment)v0).failed && v1 >= ((Segment)v0).start && v1 <= ((Segment)v0).end) {
                                    v1 = ((Segment)v0).end + 1;
                                    goto label_20;
                                }

                                goto label_63;
                            }
                            else {
                                goto label_48;
                            }
                        }
                    }
                    else {
                        goto label_22;
                    }

                    goto label_23;
                }

            label_63:
                ++v3;
                goto label_45;
            label_48:
                v0_1 = 1;
                goto label_23;
            label_22:
                v0_1 = 0;
            label_23:
                if(v0_1 == 0 && MainActivity.size_total != (((long)0))) {
                    int v1_1;
                    for(v1_1 = 0; v1_1 < MainActivity.segments.size(); ++v1_1) {
                        v0 = MainActivity.segments.get(v1_1);
                        if(!((Segment)v0).failed && !((Segment)v0).done) {
                            goto label_87;
                        }
                    }

                    v4 = 1;
                label_87:
                    if(v4 != 0) {
                        return -2;
                    }

                    return -1;
                }

                for(v3 = 0; v3 < MainActivity.segments.size(); ++v3) {
                    v0 = MainActivity.segments.get(v3);
                    if(!((Segment)v0).failed && ((Segment)v0).start > v1 && (v5 == (((long)0)) || ((Segment)v0).start <= v5)) {
                        v5 = ((Segment)v0).start - (((long)1));
                    }
                }

                if(v5 == (((long)0))) {
                    v5 = (((long)MainActivity.buffer_size)) + v1 - (((long)1));
                }

                if(v5 >= MainActivity.size_total) {
                    v5 = MainActivity.size_total - (((long)1));
                }

            label_10:
                MainActivity.segments.add(new Segment(v1, v5));
                v0_1 = MainActivity.segments.size() - 1;
            }

            return v0_1;
        }

        private static byte[] getStringFromUrl(Context arg9, String arg10, long arg11, long arg13, int arg15, boolean arg16) {
            Class v4_2;
            Class v5;
            int v4;
            byte[] v0;
            InputStream v3 = GetService.getInputStreamFromUrl(arg10, arg11, arg13, arg15, arg16);
            if(v3 == null) {
                Log.e("SuperDownload", "Could not connect");
                v0 = new byte[0];
                return v0;
            }

            byte[] v2 = new byte[(((int)(arg13 - arg11))) + 1];
            int v1 = 0;
            int v0_1 = 0;
            try {
                do {
                label_14:
                    v4 = v3.read(v2, v1, v2.length - v1);
                    if(v4 > 0) {
                        break;
                    }

                    goto label_18;
                }
                while(true);
            }
            catch(IOException v3_1) {
                goto label_74;
            }

            v1 += v4;
            try {
                if(SystemClock.elapsedRealtime() - MainActivity.time_update > (((long)800))) {
                    try {
                        v5 = Class.forName("it.opbyte.superdownload.MainActivity$GetService");
                    }
                    catch(ClassNotFoundException v3_2) {
                        throw new NoClassDefFoundError(((Throwable)v3_2).getMessage());
                    }

                    Intent v4_1 = new Intent(arg9, v5);
                    v4_1.setAction(".ACTION_GETURL_PARTIAL");
                    v4_1.putExtra("url", arg10);
                    v4_1.putExtra("size", v1 - v0_1);
                    arg9.startService(v4_1);
                    v0_1 = v1;
                }

                if(MainActivity.status == 0) {
                    goto label_18;
                }

                if(MainActivity.status != 8) {
                    goto label_14;
                }
            }
            catch(IOException v3_1) {
            label_74:
                v3_1.printStackTrace();
                v0_1 = v0_1;
                v1 = v1;
            }

        label_18:
            if(v1 > v0_1) {
                try {
                    v4_2 = Class.forName("it.opbyte.superdownload.MainActivity$GetService");
                }
                catch(ClassNotFoundException v0_2) {
                    throw new NoClassDefFoundError(((Throwable)v0_2).getMessage());
                }

                Intent v3_3 = new Intent(arg9, v4_2);
                v3_3.setAction(".ACTION_GETURL_PARTIAL");
                v3_3.putExtra("url", arg10);
                v3_3.putExtra("size", v1 - v0_1);
                arg9.startService(v3_3);
            }

            if(v1 != v2.length) {
                v0 = new byte[v1];
                System.arraycopy(v2, 0, v0, 0, v1);
            }
            else {
                v0 = v2;
            }

            return v0;
        }

        private static void getUrl(Context arg11, String arg12, boolean arg13, long arg14, long arg16, int arg18) {
            boolean v1 = !arg13 || MainActivity.dual_mode <= 0 ? false : true;
            Thread v10 = new Thread(new Runnable2(v1, arg11, arg12, arg14, arg16, arg18, arg13));
            v10.setPriority(4);
            v10.start();
        }

        @Override 
        public IBinder onBind(Intent arg2) {
            return null;
        }

        /*复写Servie父类的回调函数*/
        @Override 
        public int onStartCommand(Intent intent, int arg19, int arg20) {
            Class v6;
            Class v5_1;
            boolean v4;
            int v3_2;
            String v1_2;
            int v1;
            /*待下载的url*/
            String url = intent.getStringExtra("url");
            Object v9 = null;
            int v8 = 0;
            /*若传入的intent的action为.ACTION_START*/
            if(intent.getAction().equals(".ACTION_START")) {
                if(MainActivity.status != 0 && MainActivity.status != 2 && MainActivity.status != 5 && MainActivity.status != 6 && MainActivity.status != 7) {
                    v1 = 2;
                    return v1;
                }

                MainActivity.time_start = SystemClock.elapsedRealtime();
                /*得到解析后的url*/
                Uri url_parsed = Uri.parse(url);
                if(url_parsed.getScheme() != null && url_parsed.getHost() != null) {
                    MainActivity.temp_file_dir = new StringBuffer().append(new StringBuffer().append(MainActivity.temp_dir).append("/").toString()).append(Uri.encode(url)).toString();
                    File v3 = new File(MainActivity.temp_file_dir);
                    MainActivity.file_name = url_parsed.getLastPathSegment();
                    if(MainActivity.file_name == null || (MainActivity.file_name.equals(""))) {
                        MainActivity.file_name = "unknown";
                    }

                    MainActivity.size_done = ((long)0);
                    MainActivity.size_restarted = ((long)0);
                    MainActivity.segments.clear();
                    if(!v3.mkdir()) {
                        MainActivity.LoadPartial(MainActivity.temp_file_dir);
                    }

                    MainActivity.url_orig = url;
                    MainActivity.status = 3;
                    MainActivity.size_total = ((long)0);
                    MainActivity.speed_curr = ((float)0);
                    MainActivity.mime_type = "";
                    MainActivity.failed_mobile = 0;
                    MainActivity.failed_main = 0;
                    if(MainActivity.dual_mode == 2) {
                        MainActivity.dual_mode = 1;
                    }

                    MainActivity.progressUpdate(true);
                    if((MainActivity.show_notification) && MainActivity.notification != null) {
                        this.startForeground(0, MainActivity.notification);
                    }

                    v8 = GetService.getSegment();
                    v9 = MainActivity.segments.get(v8);
                    GetService.getUrl(this, url, false, ((Segment)v9).start, ((Segment)v9).end, v8);
                    MainActivity.nthread = 1;
                    MainActivity.nthread_mobile = 0;
                    goto label_41;
                }

                Log.e("SuperDownload", "Invalid url!");
                MainActivity.status = 6;
                MainActivity.progressUpdate(true);
            }

        label_41:
            if(intent.getAction().equals(".ACTION_GETURL_PARTIAL")) {
                v1 = intent.getIntExtra("size", -1);
                if(v1 > 0) {
                    if(MainActivity.status != 1 && MainActivity.status != 3 && MainActivity.status != 4 && MainActivity.status != 8) {
                        goto label_75;
                    }

                    MainActivity.size_done += ((long)v1);
                    if(MainActivity.status == 3) {
                        MainActivity.status = 1;
                    }

                    MainActivity.fails = 0;
                    MainActivity.progressUpdate(false);
                }
            }

        label_75:
            if(intent.getAction().equals(".ACTION_RESUME")) {
                String v3_1 = "SuperDownload";
                v1_2 = MainActivity.status == 0 ? "Stopped!" : "Resuming";
                Log.d(v3_1, v1_2);
                MainActivity.progressUpdate(true);
                if(MainActivity.status == 0) {
                    MainActivity.notificationManager.cancel(0);
                    return 2;
                }

                MainActivity.status = 3;
                MainActivity.speed_curr = ((float)0);
                v8 = GetService.getSegment();
                v3_2 = 1;
            }
            else {
                v3_2 = 0;
            }

            if(intent.getAction().equals(".ACTION_GETURL_DONE")) {
                long v5 = intent.getLongExtra("start", ((long)-1));
                long v7 = intent.getLongExtra("end", ((long)-1));
                v4 = intent.getBooleanExtra("mobile", false);
                int v10 = intent.getIntExtra("segment", -1);
                --MainActivity.nthread;
                if(MainActivity.dual_mode > 0 && (v4) && MainActivity.nthread_mobile > 0) {
                    --MainActivity.nthread_mobile;
                }

                if(MainActivity.size_total >= MainActivity.max_size && MainActivity.notificationManager != null) {
                    Notification v1_3 = new Notification(2130837505, this.getString(2131034115), System.currentTimeMillis());
                    ((Notification)v1_2).flags |= 16;
                    v1_3.setLatestEventInfo(this, this.getString(2131034116), this.getString(2131034117), PendingIntent.getActivity(this, 0, new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=it.opbyte.superdownload")), 268435456));
                    MainActivity.notificationManager.notify(101, v1_3);
                }

                if(MainActivity.dual_mode == 1 && (MainActivity.failed_mobile > MainActivity.nthread_mobile + 1 || MainActivity.failed_main > 0)) {
                    Log.e("SuperDownload", "Disabling dual mode because no dual interfaces");
                    MainActivity.dual_mode = 2;
                }

                if(v5 == (((long)0))) {
                    StatFs v1_4 = new StatFs(MainActivity.download_dir);
                    if((((double)(MainActivity.size_total * (((long)2))))) > (((double)v1_4.getAvailableBlocks())) * (((double)v1_4.getBlockSize()))) {
                        Log.e("SuperDownload", "Not enough free space");
                        MainActivity.status = 7;
                    }
                }

                if(MainActivity.status != 0 && MainActivity.status != 5 && MainActivity.status != 7) {
                    if(v7 - v5 >= (((long)0))) {
                        if(MainActivity.nthread > 0) {
                            MainActivity.progressUpdate(true);
                            ++MainActivity.speed_count;
                            if(MainActivity.speed_count % (MainActivity.nthread + 1) == MainActivity.nthread) {
                                v5 = SystemClock.elapsedRealtime();
                                if(MainActivity.last_update > (((long)0))) {
                                    MainActivity.speed_curr = (((float)(MainActivity.size_done - MainActivity.last_size))) / (((float)(v5 - MainActivity.last_update)));
                                }

                                if(MainActivity.speed_curr < (((float)0))) {
                                    MainActivity.speed_curr = ((float)0);
                                }

                                MainActivity.last_update = v5;
                                MainActivity.last_size = MainActivity.size_done;
                            }
                        }

                        MainActivity.segments.get(v10).end = v7;
                        MainActivity.segments.get(v10).done = true;
                    }
                    else {
                        MainActivity.segments.get(v10).failed = true;
                        ++MainActivity.fails;
                        if(MainActivity.fails <= 14) {
                            goto label_364;
                        }

                        MainActivity.status = 5;
                        MainActivity.progressUpdate(true);
                        return 2;
                    }

                label_364:
                    if(MainActivity.status == 8) {
                        Log.d("SuperDownload", "Paused");
                        MainActivity.progressUpdate(true);
                        return 2;
                    }

                    if(MainActivity.size_total > (((long)0))) {
                        v8 = GetService.getSegment();
                        goto label_395;
                    }

                    v8 = -2;
                    goto label_395;
                }

                v2 = "SuperDownload";
                v1_2 = MainActivity.status == 0 ? "Stopped!" : "Failed!";
                Log.d(v2, v1_2);
                MainActivity.progressUpdate(true);
                if(MainActivity.status == 0) {
                    MainActivity.notificationManager.cancel(0);
                }

                MainActivity.DeleteTemp(MainActivity.temp_file_dir);
                return 2;
            }
            else {
                v4 = false;
            }

        label_395:
            if((intent.getAction().equals(".ACTION_GETURL_DONE")) || v3_2 != 0) {
                Object v1_5 = v8 >= 0 ? MainActivity.segments.get(v8) : v9;
                if(MainActivity.nthread == 0 && v8 > 0) {
                    try {
                        v5_1 = Class.forName("it.opbyte.superdownload.MainActivity$GetService");
                    }
                    catch(ClassNotFoundException v1_6) {
                        throw new NoClassDefFoundError(((Throwable)v1_6).getMessage());
                    }

                    Intent v3_3 = new Intent(this, v5_1);
                    v3_3.setAction(".ACTION_GETURL");
                    v3_3.putExtra("segment", v8);
                    v3_3.putExtra("url", v2);
                    v3_3.putExtra("start", ((Segment)v1_5).start);
                    v3_3.putExtra("end", ((Segment)v1_5).end);
                    v3_3.putExtra("mobile", true);
                    this.startService(v3_3);
                    ++MainActivity.nthread;
                    if(MainActivity.dual_mode > 0) {
                        ++MainActivity.nthread_mobile;
                    }

                    v8 = GetService.getSegment();
                    if(v8 <= 0) {
                        goto label_440;
                    }

                    v1_5 = MainActivity.segments.get(v8);
                }

            label_440:
                for(v3_2 = 0; v8 >= 0; v3_2 = v5_3) {
                    try {
                        v6 = Class.forName("it.opbyte.superdownload.MainActivity$GetService");
                    }
                    catch(ClassNotFoundException v1_6) {
                        throw new NoClassDefFoundError(((Throwable)v1_6).getMessage());
                    }

                    Intent v5_2 = new Intent(this, v6);
                    v5_2.setAction(".ACTION_GETURL");
                    v5_2.putExtra("segment", v8);
                    v5_2.putExtra("url", v2);
                    v5_2.putExtra("start", ((Segment)v1_5).start);
                    v5_2.putExtra("end", ((Segment)v1_5).end);
                    v5_2.putExtra("mobile", v4);
                    this.startService(v5_2);
                    ++MainActivity.nthread;
                    if(MainActivity.dual_mode > 0 && (v4)) {
                        ++MainActivity.nthread_mobile;
                    }

                    if(!MainActivity.multi_thread) {
                        break;
                    }

                    if(MainActivity.nthread >= 7) {
                        break;
                    }

                    int v5_3 = v3_2 + 1;
                    if(v3_2 >= 1) {
                        break;
                    }

                    v8 = GetService.getSegment();
                    if(v8 > 0) {
                        v1_5 = MainActivity.segments.get(v8);
                    }
                }

                if(v8 == -1 || v8 == -2) {
                    Log.d("SuperDownload", "Finishing");
                }

                if(v8 != -2) {
                    goto label_460;
                }

                Log.d("SuperDownload", "Compacting");
                MainActivity.status = 4;
                MainActivity.progressUpdate(true);
                GetService.compactSegments(this, v2);
            }

        label_460:
        /*Send to system downloader*/
            if(intent.getAction().equals(".ACTION_FINISHED")) {
                MainActivity.status = 2;
                MainActivity.progressUpdate(true);
                if(MainActivity.dmanager == null) {
                    goto label_488;
                }

                Log.d("SuperDownload", new StringBuffer().append("Sending to download manager: ").append(MainActivity.file_down).toString());
                try {
                    MainActivity.dmanager.addCompletedDownload(MainActivity.file_name, MainActivity.file_name, true, MainActivity.mime_type, MainActivity.file_down, MainActivity.size_total, false);
                }
                catch(Exception v1_7) {
                    v1_7.printStackTrace();
                }
            }

        label_488:
            if(intent.getAction().equals(".ACTION_GETURL")) {
                GetService.getUrl(this, v2, intent.getBooleanExtra("mobile", false), intent.getLongExtra("start", ((long)-1)), intent.getLongExtra("end", ((long)-1)), intent.getIntExtra("segment", -1));
            }

            return 2;
        }
    }

    public class Segment {
        public boolean done;
        public long end;
        public boolean failed;
        public long start;

        public Segment(long arg4, long arg6) {
            super();
            this.start = ((long)0);
            this.end = ((long)0);
            this.done = false;
            this.failed = false;
            this.start = arg4;
            this.end = arg6;
        }
    }

    public class segmentComparator implements Comparator {
        public segmentComparator() {
            super();
        }

        public int compare(Segment arg5, Segment arg6) {
            int v0;
            if(arg5.start == arg6.start) {
                v0 = 0;
            }
            else if(arg5.start < arg6.start) {
                v0 = -1;
            }
            else {
                v0 = 1;
            }

            return v0;
        }

        public int compare(Object arg2, Object arg3) {
            return this.compare(((Segment)arg2), ((Segment)arg3));
        }
    }

    public static final int BUFFER_SIZE = 1048576;
    public static final String DIR_DOWN = null;
    public static final String DIR_STOR = null;
    public static final String DIR_TEMP = ".SuperDownloadTemp";
    public static final boolean FROYO = false;
    public static final int MAX_FAILS = 14;
    public static final int MAX_SPAWNED_THREADS = 1;
    public static final int MAX_THREADS = 7;
    public static final String TAG_LOG = "SuperDownload";
    public static boolean big_chunks;
    public static int buffer_size;
    public static boolean clear;
    public static DownloadManager dmanager;
    public static String download_dir;
    public static int dual_mode;
    public static int failed_main;
    public static int failed_mobile;
    static int fails;
    static String file_down;
    static String file_name;
    private static final NumberFormat formatterSpeed;
    static long last_size;
    static long last_update;
    private static SharedPreferences$OnSharedPreferenceChangeListener listener;
    public static boolean lite;
    public SharedPreferences mPrefs;
    public static long max_size;
    static String mime_type;
    public static boolean multi_thread;
    public static View no_files;
    static Notification notification;
    public static NotificationManager notificationManager;
    public static int nthread;
    public static int nthread_mobile;
    public static View progress;
    public static ProgressBar progressBar;
    public static BroadcastReceiver receiver;
    public static View row_dual_mode;
    public static View row_threads;
    public static View row_time;
    static List segments;
    public static boolean show_notification;
    static long size_done;
    static long size_restarted;
    static long size_total;
    static int speed_count;
    static float speed_curr;
    static int status;
    public static String temp_dir;
    static String temp_file_dir;
    public static TextView text_dual_mode;
    public static TextView text_expand;
    public static TextView text_filename;
    public static TextView text_progress;
    public static TextView text_speed;
    public static TextView text_status;
    public static TextView text_threads;
    public static TextView text_time;
    static long time_start;
    static long time_update;
    static String url_orig;

    static final {
        View v5 = null;
        MainActivity.DIR_STOR = Environment.getExternalStorageDirectory().getPath();
        MainActivity.DIR_DOWN = new StringBuffer().append(MainActivity.DIR_STOR).append("/Download").toString();
        boolean v0 = Build$VERSION.SDK_INT >= 9 ? false : true;
        MainActivity.FROYO = v0;
        MainActivity.formatterSpeed = new DecimalFormat("#,##0.00");
        MainActivity.lite = true;
        MainActivity.max_size = ((long)52428800);
        MainActivity.progress = v5;
        MainActivity.no_files = v5;
        MainActivity.row_time = v5;
        MainActivity.row_threads = v5;
        MainActivity.row_dual_mode = v5;
        MainActivity.progressBar = ((ProgressBar)v5);
        MainActivity.text_status = ((TextView)v5);
        MainActivity.text_filename = ((TextView)v5);
        MainActivity.text_progress = ((TextView)v5);
        MainActivity.text_time = ((TextView)v5);
        MainActivity.text_speed = ((TextView)v5);
        MainActivity.text_threads = ((TextView)v5);
        MainActivity.text_expand = ((TextView)v5);
        MainActivity.text_dual_mode = ((TextView)v5);
        MainActivity.notificationManager = ((NotificationManager)v5);
        MainActivity.multi_thread = true;
        MainActivity.big_chunks = true;
        MainActivity.show_notification = true;
        MainActivity.clear = true;
        MainActivity.download_dir = MainActivity.DIR_DOWN;
        MainActivity.temp_dir = new StringBuffer().append(new StringBuffer().append(MainActivity.download_dir).append("/").toString()).append(".SuperDownloadTemp").toString();
        MainActivity.dual_mode = 1;
        MainActivity.buffer_size = 1048576;
        MainActivity.nthread = 0;
        MainActivity.nthread_mobile = 0;
        MainActivity.failed_mobile = 0;
        MainActivity.failed_main = 0;
        MainActivity.dmanager = ((DownloadManager)v5);
        MainActivity.receiver = ((BroadcastReceiver)v5);
        MainActivity.notification = ((Notification)v5);
        MainActivity.size_total = ((long)0);
        MainActivity.time_start = ((long)0);
        MainActivity.time_update = ((long)0);
        MainActivity.size_done = ((long)0);
        MainActivity.last_update = ((long)0);
        MainActivity.last_size = ((long)0);
        MainActivity.size_restarted = ((long)0);
        MainActivity.speed_curr = ((float)0);
        MainActivity.url_orig = "";
        MainActivity.file_name = "";
        MainActivity.file_down = "";
        MainActivity.mime_type = "";
        MainActivity.temp_file_dir = "";
        MainActivity.segments = new ArrayList();
        MainActivity.status = 0;
        MainActivity.speed_count = 0;
        MainActivity.fails = 0;
    }

    public MainActivity() {
        super();
        this.mPrefs = null;
    }
	/*删除临时文件*/
    static void DeleteTemp(String arg4) {
        File v1 = new File(arg4);
        if(v1.exists()) {
            File[] v2 = v1.listFiles();
            int v0;
            for(v0 = 0; v0 < v2.length; ++v0) {
                v2[v0].delete();
            }

            v1.delete();
        }
    }

    void FindPartial(Context arg7) {
        String v0_2;
        String v2 = null;
        /*即存储卡目录下的/Download/.SuperDownloadTemp/目录，是一个隐藏目录*/
        File v0 = new File(MainActivity.temp_dir);
        if(v0.exists()) {
            File[] v3 = v0.listFiles();
            int v0_1 = 0;
            while(true) {
                if(v0_1 < v3.length) {
                    File v4 = v3[v0_1];
                    if(v4.isDirectory()) {
                        v2 = v4.getName();
                        v0_2 = v4.toString();
                    }
                    else {
                        ++v0_1;
                        continue;
                    }
                }
                else {
                    goto label_11;
                }

                break;
            }
        }
        else {
        label_11:
            v0_2 = v2;
        }

        if(v2 != null && !v2.equals("")) {
            v2 = Uri.decode(v2);
            AlertDialog$Builder v3_1 = new AlertDialog$Builder(arg7);
            v3_1.setMessage(String.format(this.getResources().getString(2131034120), v2)).setPositiveButton(17039379, new tDialogInterface_OnClickListener(this, arg7, v2)).setNegativeButton(17039369, new sDialogInterface_OnClickListener(this, v0_2));
            v3_1.create().show();
        }
    }

    static void LoadPartial(String arg12) {
        File v0 = new File(arg12);
        Log.d("SuperDownload", new StringBuffer().append("LoadPartial started on ").append(arg12).toString());
        if(v0 != null) {
            File[] v5 = v0.listFiles();
            int v3;
            for(v3 = 0; v3 < v5.length; ++v3) {
                File v2 = v5[v3];
                Long v1 = Long.valueOf(v2.getName());
                Log.d("SuperDownload", new StringBuffer().append("New segment: starts at ").append(v1).toString());
                if(v1.longValue() != (((long)0))) {
                    Long v2_1 = new Long(v2.length());
                    Log.d("SuperDownload", new StringBuffer().append("size: ").append(v2_1).toString());
                    Segment v6 = new Segment(v1.longValue(), v2_1.longValue() + v1.longValue() - (((long)1)));
                    v6.done = true;
                    MainActivity.segments.add(v6);
                    MainActivity.size_done += v2_1.longValue();
                }
            }
        }

        MainActivity.size_restarted = MainActivity.size_done;
    }

    private void ReadPreferences() {
        int v0_1;
        if(this.mPrefs != null) {
            Log.d("SuperDownload", "Reading preferences");
            if(this.mPrefs.getString("download_dir", "").equals("")) {
                SharedPreferences$Editor v0 = this.mPrefs.edit();
                v0.putString("download_dir", MainActivity.DIR_DOWN);
                v0.commit();
            }

            v0_1 = this.mPrefs.getBoolean("dual_mode", true) ? 1 : 0;
            MainActivity.dual_mode = v0_1;
            MainActivity.multi_thread = this.mPrefs.getBoolean("multi_thread", true);
            MainActivity.big_chunks = this.mPrefs.getBoolean("big_chunks", true);
            MainActivity.show_notification = this.mPrefs.getBoolean("show_notification", true);
            MainActivity.download_dir = this.mPrefs.getString("download_dir", MainActivity.DIR_DOWN);
            MainActivity.temp_dir = new StringBuffer().append(new StringBuffer().append(MainActivity.download_dir).append("/").toString()).append(".SuperDownloadTemp").toString();
        }

        v0_1 = MainActivity.big_chunks ? 1048576 : 349525;
        MainActivity.buffer_size = v0_1;
    }

/* 将得到的url传入GetService，然后启动GetService*/
    public void Restart(Context arg5, String url) {
        Class v1;
        if(!url.equals("")) {
            File v0 = new File(MainActivity.download_dir);
            if(!v0.mkdir() && !v0.isDirectory()) {
                AlertDialog$Builder v0_1 = new AlertDialog$Builder(((Context)this));
                v0_1.setMessage(2131034119).setPositiveButton(17039379, new wDialogInterface_OnClickListener(this));
                v0_1.create().show();
                return;
            }

            new File(MainActivity.temp_dir).mkdir();
            try {
                v1 = Class.forName("it.opbyte.superdownload.MainActivity$GetService");
            }
            catch(ClassNotFoundException v0_2) {
                throw new NoClassDefFoundError(((Throwable)v0_2).getMessage());
            }

            Intent v0_3 = new Intent(arg5, v1);
            v0_3.setAction(".ACTION_START");
            v0_3.putExtra("url", url);
            /*启动 GetService，并传入待下载的url */
            arg5.startService(v0_3);
        }
    }

    void SendToAndroidDownloader(Context arg4, String arg5) {
        if(!arg5.equals("")) {
            if(MainActivity.FROYO) {
                this.froyoWarning(arg4);
            }
            else {
                MainActivity.dmanager.enqueue(new DownloadManager$Request(Uri.parse(arg5)));
            }
        }
    }

    static void access$1000010(MainActivity arg0) {
        arg0.ReadPreferences();
    }

    static void access$1000012(boolean arg0) {
        MainActivity.progressUpdate(arg0);
    }

    static String access$1000022(String arg1) {
        return MainActivity.getFileName(arg1);
    }

    static boolean access$1000023(Context arg1, String arg2) {
        return MainActivity.forceMobileConnectionForAddress(arg1, arg2);
    }

    static InetAddress access$1000026(boolean arg1) {
        return MainActivity.getAddressByType(arg1);
    }

    public void clear() {
        MainActivity.progressVisibility(false);
        MainActivity.notificationManager.cancel(0);
    }

    public void expandClicked(View arg4) {
        int v1 = 8;
        if(MainActivity.row_time.getVisibility() == v1) {
            MainActivity.row_time.setVisibility(0);
            MainActivity.row_threads.setVisibility(0);
            MainActivity.row_dual_mode.setVisibility(0);
            MainActivity.text_expand.setText("[ - ]");
        }
        else {
            MainActivity.row_time.setVisibility(v1);
            MainActivity.row_threads.setVisibility(v1);
            MainActivity.row_dual_mode.setVisibility(v1);
            MainActivity.text_expand.setText("[ + ]");
        }
    }

    private static boolean forceMobileConnectionForAddress(Context arg9, String arg10) {
        boolean v0_1;
        int v8 = -1;
        int v1 = 0;
        Object v0 = arg9.getSystemService("connectivity");
        if(v0 == null) {
            Log.e("SuperDownload", "ConnectivityManager is null, cannot try to force a mobile connection");
            v0_1 = false;
            return v0_1;
        }

        int v3 = 5;
        if(!ConnectivityManager.isNetworkTypeValid(v3)) {
            Log.e("SuperDownload", "Invalid network type");
            return false;
        }

        NetworkInfo v4 = ((ConnectivityManager)v0).getNetworkInfo(v3);
        if(v4 == null) {
            Log.e("SuperDownload", "Network type not supported");
            return false;
        }

        NetworkInfo$State v4_1 = v4.getState();
        Log.d("SuperDownload", new StringBuffer().append("network state: ").append(v4_1).toString());
        if(v4_1.compareTo(NetworkInfo$State.CONNECTED) != 0 && v4_1.compareTo(NetworkInfo$State.CONNECTING) != 0) {
            int v4_2 = ((ConnectivityManager)v0).startUsingNetworkFeature(0, "enableHIPRI");
            if(v8 == v4_2) {
                Log.e("SuperDownload", "Wrong result of startUsingNetworkFeature");
                v0_1 = false;
            }
            else {
                MainActivity.setRouting(arg9);
                if(v4_2 == 0) {
                    Log.d("SuperDownload", "No need to perform additional network settings");
                    v0_1 = true;
                }
                else {
                    int v2 = MainActivity.lookupHost(Uri.parse(arg10).getHost());
                    if(v8 == v2) {
                        Log.e("SuperDownload", "Wrong host address transformation, result was -1");
                        return false;
                    }

                    while(v1 < 30) {
                        try {
                            if(((ConnectivityManager)v0).getNetworkInfo(v3).getState().compareTo(NetworkInfo$State.CONNECTED) != 0) {
                                goto label_78;
                            }

                            break;
                        }
                        catch(InterruptedException v1_1) {
                            goto label_86;
                        }

                    label_78:
                        long v4_3 = ((long)500);
                        try {
                            Thread.sleep(v4_3);
                            ++v1;
                            continue;
                        }
                        catch(InterruptedException v1_1) {
                        label_86:
                            break;
                        }
                    }

                    v0_1 = ((ConnectivityManager)v0).requestRouteToHost(v3, v2);
                    if(v0_1) {
                        return v0_1;
                    }

                    Log.e("SuperDownload", "Wrong requestRouteToHost result: expected true, but was false");
                }
            }
        }
        else {
            v0_1 = true;
        }

        return v0_1;
    }

    private static String formatTime(long arg13) {
        int v12 = 3600;
        int v11 = 10;
        int v10 = 60;
        StringBuffer v1 = new StringBuffer();
        StringBuffer v2 = new StringBuffer();
        StringBuffer v3 = new StringBuffer();
        StringBuffer v4 = new StringBuffer();
        StringBuffer v5 = new StringBuffer();
        String v0 = arg13 / (((long)v12)) > (((long)0)) ? new StringBuffer().append(Long.toString(arg13 / (((long)v12)))).append(":").toString() : "";
        v5 = v5.append(v0);
        v0 = arg13 / (((long)v10)) % (((long)v10)) < (((long)v11)) ? "0" : "";
        v2 = v2.append(v3.append(v4.append(v5.append(v0).toString()).append(Long.toString(arg13 / (((long)v10)) % (((long)v10)))).toString()).append(":").toString());
        v0 = arg13 % (((long)v10)) < (((long)v11)) ? "0" : "";
        return v1.append(v2.append(v0).toString()).append(Long.toString(arg13 % (((long)v10)))).toString();
    }

    public void froyoWarning(Context arg5) {
        AlertDialog$Builder v0 = new AlertDialog$Builder(arg5);
        v0.setMessage(2131034118).setPositiveButton(17039379, new vDialogInterface_OnClickListener(this));
        v0.create().show();
    }

    private static InetAddress getAddressByName(String arg6) {
        InetAddress v0_3;
        Object v0_2;
        try {
            NetworkInterface v0_1 = NetworkInterface.getByName(arg6);
            if(v0_1 != null) {
                Enumeration v1 = v0_1.getInetAddresses();
                do {
                label_5:
                    if(!v1.hasMoreElements()) {
                        goto label_2;
                    }

                    v0_2 = v1.nextElement();
                    if(((InetAddress)v0_2).isLoopbackAddress()) {
                        goto label_5;
                    }
                }
                while(!(v0_2 instanceof Inet4Address));

                Log.d("SuperDownload", new StringBuffer().append(new StringBuffer().append(new StringBuffer().append("Found address for interface ").append(arg6).toString()).append(": ").toString()).append(((InetAddress)v0_2).getHostAddress().toString()).toString());
            }
            else {
            label_2:
                v0_3 = null;
            }

            return v0_3;
        }
        catch(SocketException v0) {
            Log.e("SuperDownload", v0.toString());
            goto label_2;
        }
    }

    private static InetAddress getAddressByType(boolean arg7) {
        String[] v0 = new String[]{"uwbr0", "rmnet0", "cdma_rmnet0", "rmnet_usb0", "ppp0", "p2p0"};
        String[] v1 = new String[]{"wlan0", "eth0"};
        InetAddress v3 = null;
        if(!arg7) {
            v0 = v1;
        }

        int v1_1;
        for(v1_1 = 0; v1_1 < v0.length; ++v1_1) {
            String v3_1 = v0[v1_1];
            Log.d("SuperDownload", new StringBuffer().append("Probing interface ").append(v3_1).toString());
            v3 = MainActivity.getAddressByName(v3_1);
            if(v3 != null) {
                break;
            }
        }

        if(v3 == null) {
            String v1_2 = "SuperDownload";
            StringBuffer v2 = new StringBuffer();
            StringBuffer v4 = new StringBuffer().append("No address found for ");
            String v0_1 = arg7 ? "mobile" : "main";
            Log.e(v1_2, v2.append(v4.append(v0_1).toString()).append(" interface").toString());
            if(arg7) {
                ++MainActivity.failed_mobile;
                return v3;
            }

            ++MainActivity.failed_main;
        }
        else {
            if(arg7) {
                MainActivity.failed_mobile = 0;
                return v3;
            }

            MainActivity.failed_main = 0;
        }

        return v3;
    }

    private static String getFileName(String arg7) {
        String v2_1;
        String v0;
        int v1 = 0;
        int v2 = arg7.lastIndexOf(".");
        if(v2 != -1) {
            v0 = arg7.substring(v2 + 1);
            arg7 = arg7.substring(0, v2);
        }
        else {
            v0 = "";
        }

        do {
            StringBuffer v3 = new StringBuffer();
            StringBuffer v4 = new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(MainActivity.download_dir).append("/").toString()).append(arg7).toString());
            v2_1 = v1 > 0 ? new StringBuffer().append(new StringBuffer().append(" (").append(v1).toString()).append(")").toString() : "";
            v3 = v3.append(v4.append(v2_1).toString());
            v2_1 = v0.length() > 0 ? new StringBuffer().append(".").append(v0).toString() : "";
            v2_1 = v3.append(v2_1).toString();
            ++v1;
        }
        while(new File(v2_1).exists());

        return v2_1;
    }

    private static String getGateway(Context arg1) {
        return MainActivity.intToIp(arg1.getSystemService("wifi").getDhcpInfo().gateway);
    }

    private static String intToIp(int arg7) {
        return new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(arg7 & 255).append(".").toString()).append(arg7 >> 8 & 255).toString()).append(".").toString()).append(arg7 >> 16 & 255).toString()).append(".").toString()).append(arg7 >> 24 & 255).toString();
    }

    private static int lookupHost(String arg3) {
        InetAddress v0_1;
        try {
            v0_1 = InetAddress.getByName(arg3);
        }
        catch(UnknownHostException v0) {
            int v0_2 = -1;
            return v0_2;
        }

        byte[] v0_3 = v0_1.getAddress();
        return v0_3[0] & 255 | ((v0_3[3] & 255) << 24 | (v0_3[2] & 255) << 16 | (v0_3[1] & 255) << 8);
    }

    @Override 
    public void onBackPressed() {
        this.moveTaskToBack(true);
    }

    /*Activity的入口*/
    @Override 
    public void onCreate(Bundle arg6) {
        Class v1;
        super.onCreate(arg6);
        this.setContentView(2130903041);
        MainActivity.lite = this.getPackageName().contains("_lite");
        if(!MainActivity.lite) {
            MainActivity.max_size = 9223372036854775807L;
        }

        MainActivity.progress = this.findViewById(2131296258);
        MainActivity.no_files = this.findViewById(2131296271);
        MainActivity.progressBar = this.findViewById(2131296269);
        MainActivity.text_status = this.findViewById(2131296259);
        MainActivity.text_filename = this.findViewById(2131296260);
        MainActivity.text_progress = this.findViewById(2131296261);
        MainActivity.text_time = this.findViewById(2131296264);
        MainActivity.text_speed = this.findViewById(2131296262);
        MainActivity.text_threads = this.findViewById(2131296268);
        MainActivity.text_dual_mode = this.findViewById(2131296266);
        MainActivity.row_time = this.findViewById(2131296263);
        MainActivity.row_threads = this.findViewById(2131296267);
        MainActivity.row_dual_mode = this.findViewById(2131296265);
        MainActivity.text_expand = this.findViewById(2131296270);
        this.findViewById(2131296272).setAlpha(65);
        this.mPrefs = PreferenceManager.getDefaultSharedPreferences(((Context)this));
        this.ReadPreferences();
        Intent v0 = arg6 == null ? this.getIntent() : null;
        this.onNewIntent(v0);
        if(MainActivity.notification == null) {
            MainActivity.notification = new Notification(2130837505, "", System.currentTimeMillis());
            MainActivity.notification.flags |= 2;
            MainActivity.notification.contentView = new RemoteViews(this.getPackageName(), 2130903042);
            try {
                v1 = Class.forName("it.opbyte.superdownload.MainActivity");
            }
            catch(ClassNotFoundException v0_1) {
                throw new NoClassDefFoundError(((Throwable)v0_1).getMessage());
            }

            MainActivity.notification.contentIntent = PendingIntent.getActivity(((Context)this), 0, new Intent(((Context)this), v1), 268435456);
        }

        if(MainActivity.notificationManager == null) {
            MainActivity.notificationManager = this.getSystemService("notification");
        }

        if(!MainActivity.FROYO) {
            MainActivity.dmanager = this.getSystemService("download");
        }

        if(!MainActivity.FROYO && MainActivity.receiver == null) {
            MainActivity.receiver = new MyBroadcastReceiver(this);
            this.registerReceiver(MainActivity.receiver, new IntentFilter("android.intent.action.DOWNLOAD_NOTIFICATION_CLICKED"));
        }
    }

    @Override 
    public boolean onCreateOptionsMenu(Menu arg3) {
        this.getMenuInflater().inflate(2131230720, arg3);
        return 1;
    }

    @Override 
    protected void onDestroy() {
        super.onDestroy();
        if(MainActivity.receiver != null) {
            this.unregisterReceiver(MainActivity.receiver);
            MainActivity.receiver = null;
        }
    }

    @Override 
    public void onNewIntent(Intent newIntent) {
        String v0_1;
        super.onNewIntent(newIntent);
        this.setIntent(newIntent);
        String v3 = "";
        boolean v0 = MainActivity.clear ? false : true;
        MainActivity.progressVisibility(v0);
        if(!MainActivity.clear) {
            MainActivity.progressUpdate(false);
        }

        if(newIntent == null || (newIntent.getFlags() & 1048576) != 0 || newIntent.getAction() == null) {
        label_76:
            v0_1 = v3;
        }
        else {
            if((newIntent.getAction().equals("android.intent.action.SEND")) && (newIntent.hasExtra("android.intent.extra.TEXT"))) {
                v0_1 = newIntent.getStringExtra("android.intent.extra.TEXT");
                goto label_28;
            }

            if(!newIntent.getAction().equals("android.intent.action.VIEW")) {
                goto label_76;
            }

            v0_1 = newIntent.getDataString();
        }

    label_28:
        if(MainActivity.status == 0 && (v0_1 == null || (v0_1.equals("")))) {
            this.FindPartial(((Context)this));
        }
        /*点击"确认"按钮之后结束对话框*/
        if(!v0_1.equals("")) {
            if(MainActivity.status != 0 && MainActivity.status != 2 && MainActivity.status != 5 && MainActivity.status != 6 && MainActivity.status != 7) {
                AlertDialog$Builder v0_2 = new AlertDialog$Builder(((Context)this));
                v0_2.setMessage(2131034121).setPositiveButton(17039379, new zDialogInterface_OnClickListener(this));
                v0_2.create().show();
                return;
            }

            this.Restart(((Context)this), v0_1);
            if(!MainActivity.show_notification) {
                return;
            }

            this.moveTaskToBack(true);
        }
    }

    /*右上角的菜单按钮选中之后的回调函数*/
    @Override 
    public boolean onOptionsItemSelected(MenuItem arg7) {
        boolean v0;
        Class v1;
        /*判断选中的按钮*/
        switch(arg7.getItemId()) {
        	/*显示"Enter URL to download"对话框*/
            case 2131296276: {
                AlertDialog$Builder v3 = new AlertDialog$Builder(((Context)this));
                /*设置弹框对话框的View布局*/
                View v0_2 = this.getSystemService("layout_inflater").inflate(2130903040, this.findViewById(2131296256));
                v3.setView(v0_2);
                v3.setMessage(2131034114).
                /*设置"确定"和"取消"按钮的监听器*/
                setPositiveButton(17039379, new yDialogInterface_OnClickListener(this, v0_2, ((Context)this))).
                setNegativeButton(17039369, new xDialogInterface_OnClickListener(this));
                v3.create().show();
                v0 = true;
                break;
            }
            /*显示历史下载记录*/
            case 2131296277: {
                this.showHistory(((Context)this));
                v0 = true;
                break;
            }
            /*进入设置Preference的界面*/
            case 2131296278: {
                try {
                    v1 = Class.forName("it.opbyte.superdownload.Preferences");
                }
                catch(ClassNotFoundException v0_1) {
                    throw new NoClassDefFoundError(((Throwable)v0_1).getMessage());
                }
                /*启动 Preferences这个Activity*/
                this.startActivity(new Intent(((Context)this), v1));
                /*设置点击Preferences选项的监听器为MainActivity的listener对象*/
                MainActivity.listener = new MyOnSharedPreferenceChangeListener(this);
                this.mPrefs.registerOnSharedPreferenceChangeListener(MainActivity.listener);
                v0 = true;
                break;
            }
            default: {
                v0 = super.onOptionsItemSelected(arg7);
                break;
            }
        }

        return v0;
    }

/*在下载过程中，点击进度条触发*/
    public void progressClicked(View arg4) {
        int v0;
        AlertDialog$Builder v1 = new AlertDialog$Builder(((Context)this));
        switch(MainActivity.status) {
            case 2: {
                v0 = 2131099648;
                break;
            }
            case 1: 
            case 3: 
            case 4: {
                v0 = 2131099649;
                break;
            }
            case 8: {
                v0 = 2131099650;
                break;
            }
            default: {
                v0 = 2131099651;
                break;
            }
        }

        v1.setItems(v0, new uDialogInterface_OnClickListener(this, ((Context)this)));
        v1.create().show();
    }

/*更新进度*/
    private static void progressUpdate(boolean arg22) {
        String[] v7 = new String[]{"Stopped", "Downloading", "Finished", "Starting", "Finishing", "Failed", "Invalid URL", "Not enough free space", "Paused"};
        int[] v8 = new int[]{-256, -16711681, -16711936, -16711681, -16711681, -65536, -65536, -65536, -10066330};
        String[] v9 = new String[]{"Disabled", "Enabled", "Suspended"};
        int v3 = 0;
        String v2 = "";
        MainActivity.time_update = SystemClock.elapsedRealtime();
        if(MainActivity.status == 3 || MainActivity.status == 6) {
            MainActivity.progressVisibility(true);
        }

        if(MainActivity.text_filename != null) {
            long v10 = MainActivity.time_update - MainActivity.time_start + (((long)1));
            long v2_1 = MainActivity.size_done;
            if(v2_1 > MainActivity.size_total) {
                v2_1 = MainActivity.size_total;
            }

            float v12 = (((float)(MainActivity.size_done - MainActivity.size_restarted))) / (((float)v10));
            long v13 = ((long)((((float)(MainActivity.size_total - v2_1))) / v12 / (((float)1000))));
            int v6 = Math.round((((float)v2_1)) / (((float)MainActivity.size_total)) * (((float)100)));
            int v4 = MainActivity.nthread - MainActivity.nthread_mobile + 1;
            if(v4 < 0) {
                v4 = 0;
            }

            String v5 = new StringBuffer().append(new StringBuffer().append(v7[MainActivity.status]).append(": ").toString()).append(MainActivity.file_name).toString();
            MainActivity.text_status.setText(v7[MainActivity.status]);
            MainActivity.text_status.setTextColor(v8[MainActivity.status]);
            MainActivity.text_filename.setText(MainActivity.file_name);
            MainActivity.text_progress.setText(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(v2_1 / (((long)1024))).append("/").toString()).append(MainActivity.size_total / (((long)1024))).toString()).append(" KB, ").toString()).append(v6).toString()).append("%").toString());
            v2 = new StringBuffer().append(MainActivity.formatTime(v10 / (((long)1000)))).append(" elapsed").toString();
            if(v13 > (((long)0)) && v13 < (((long)100000))) {
                v2 = new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(v2).append(", ").toString()).append(MainActivity.formatTime(v13)).toString()).append(" left").toString();
            }

            MainActivity.text_time.setText(((CharSequence)v2));
            MainActivity.text_speed.setText(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(MainActivity.formatterSpeed.format(((double)MainActivity.speed_curr))).append(" KB/s curr, ").toString()).append(MainActivity.formatterSpeed.format(((double)v12))).toString()).append(" KB/s avg").toString());
            MainActivity.text_threads.setText(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(v4).append(" main, ").toString()).append(MainActivity.nthread_mobile).toString()).append(" mobile").toString());
            MainActivity.text_dual_mode.setText(v9[MainActivity.dual_mode]);
            v2 = v5;
            v3 = v6;
        }

        if(MainActivity.progressBar != null) {
            if(MainActivity.status == 4) {
                MainActivity.progressBar.setIndeterminate(true);
            }
            else {
                MainActivity.progressBar.setIndeterminate(false);
                MainActivity.progressBar.setProgress(v3);
            }
        }

        if((arg22) && MainActivity.notificationManager != null) {
            if(MainActivity.show_notification) {
                MainActivity.notification.contentView.setTextViewText(2131296274, ((CharSequence)v2));
                MainActivity.notification.contentView.setTextColor(2131296274, v8[MainActivity.status]);
                MainActivity.notification.tickerText = null;
                if(MainActivity.status == 0 || MainActivity.status == 2 || MainActivity.status == 5 || MainActivity.status == 6 || MainActivity.status == 7) {
                    if(!MainActivity.FROYO) {
                        MainActivity.notification.contentView.setViewVisibility(2131296275, 8);
                    }

                    MainActivity.notification.flags &= -3;
                    MainActivity.notification.flags |= 16;
                    if(MainActivity.status == 2) {
                        MainActivity.notification.tickerText = "Download finished!";
                    }

                    if(MainActivity.status != 5 && MainActivity.status != 6 && MainActivity.status != 7) {
                        goto label_326;
                    }

                    MainActivity.notification.tickerText = "Download failed!";
                }
                else {
                    MainActivity.notification.contentView.setProgressBar(2131296275, 100, v3, false);
                    MainActivity.notification.flags |= 2;
                    MainActivity.notification.flags &= -17;
                    if(MainActivity.status == 3) {
                        if(!MainActivity.FROYO) {
                            MainActivity.notification.contentView.setViewVisibility(2131296275, 0);
                        }

                        MainActivity.notification.tickerText = "Download started!";
                    }

                    if(MainActivity.status != 8) {
                        goto label_326;
                    }

                    MainActivity.notification.tickerText = "Download paused";
                }

            label_326:
                MainActivity.notificationManager.notify(0, MainActivity.notification);
            }
            else {
                MainActivity.notificationManager.cancel(0);
            }
        }
    }

/*设置进度条的可见性*/
    public static void progressVisibility(boolean arg4) {
        int v2 = 8;
        boolean v1 = false;
        if(MainActivity.progress != null) {
            View v3 = MainActivity.progress;
            int v0 = arg4 ? 0 : v2;
            v3.setVisibility(v0);
        }

        if(MainActivity.no_files != null) {
            View v0_1 = MainActivity.no_files;
            if(!arg4) {
                v2 = 0;
            }

            v0_1.setVisibility(v2);
        }

        if(!arg4) {
            v1 = true;
        }

        MainActivity.clear = v1;
    }

/*设置路由?*/
    private static void setRouting(Context arg12) {
        String v1_4;
        int v1_3;
        byte[] v9;
        IOException v1_1;
        Process v0_3;
        int v3 = 0;
        InetAddress v0 = MainActivity.getAddressByType(false);
        if(v0 == null) {
            return;
        }

        String v4 = v0.getHostAddress().toString();
        String v5 = MainActivity.getGateway(arg12);
        String v6 = new StringBuffer().append(new StringBuffer().append(" from ").append(v4).toString()).append(" tab 2511 priority 100").toString();
        String v7 = new StringBuffer().append(new StringBuffer().append(" default via ").append(v5).toString()).append(" dev wlan0 tab 2511").toString();
        String v2 = "";
        Process v1 = null;
        try {
            v0_3 = Runtime.getRuntime().exec(new String[]{"sh", "-c", "ip rule list;ip route list table 2511"});
        }
        catch(Throwable v0_1) {
            goto label_87;
        }
        catch(IOException v0_2) {
            IOException v11 = v0_2;
            v0_3 = v1;
            v1_1 = v11;
            goto label_83;
        }

        try {
            InputStream v8 = v0_3.getInputStream();
            v9 = new byte[512];
            while(true) {
            label_48:
                v1_3 = v8.read(v9, v3, v9.length - v3);
                if(v1_3 > 0) {
                    break;
                }

                goto label_52;
            }
        }
        catch(Throwable v1_2) {
            goto label_137;
        }
        catch(IOException v1_1) {
            goto label_142;
        }

        v3 = v1_3 + v3;
        goto label_48;
        try {
        label_52:
            v1_4 = new String(v9, 0, v3);
            goto label_55;
        }
        catch(Throwable v1_2) {
        label_137:
            Throwable v11_1 = v1_2;
            v1 = v0_3;
            v0_1 = v11_1;
        }
        catch(IOException v1_1) {
        label_142:
            try {
            label_83:
                v1_1.printStackTrace();
                v1_4 = v2;
            }
            catch(Throwable v1_2) {
                goto label_137;
            }

        label_55:
            v0_3.destroy();
            if((v1_4.contains(new StringBuffer().append("from ").append(v4).toString())) && (v1_4.contains(new StringBuffer().append("via ").append(v5).toString()))) {
                Log.d("SuperDownload", "No need to run su command");
                return;
            }

            String v0_4 = new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append("ip rule del").append(v6).toString()).append(";ip rule add").toString()).append(v6).toString()).append(";ip route flush table 2511;ip route add").toString()).append(v7).toString();
            try {
                Runtime.getRuntime().exec(new String[]{"su", "-c", v0_4}).waitFor();
            }
            catch(Exception v0_5) {
                Log.e("SuperDownload", "Could not run su commands", ((Throwable)v0_5));
            }

            return;
        }

    label_87:
        v1.destroy();
        throw v0_1;
    }

/*查看下载历史*/
    public void showHistory(Context arg3) {
        if(MainActivity.FROYO) {
            this.froyoWarning(arg3);
        }
        else {
            Intent v0 = new Intent("android.intent.action.VIEW_DOWNLOADS");
            v0.setFlags(268435456);
            this.startActivity(v0);
        }
    }
}


